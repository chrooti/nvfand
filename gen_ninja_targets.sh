#!/bin/bash

shopt -s nullglob
shopt -s globstar

main() {
    # avoid mixing spaces in file names with array separator
    OIFS="${IFS}"
    IFS=$'\0'

    local project
    for project in src/*; do
        if [[ -f ${project} ]]; then
            # monofile project -> name of file = name of executable

            project="${project// /$ }"
            filename=$(basename -s .c "${project}")

            echo "build out/bin/${filename}: link out/build/${filename}.o"
            echo ""
            echo "build ${filename}: phony out/bin/${filename}"
            echo "default out/bin/${filename}"
            echo ""
            echo "build out/build/${filename}.o: cc ${project}"
            echo ""
        else
            # multifile project -> name of dir = name of executable

            local project_name
            project_name=$(basename "${project}")

            local filenames=()
            local filename
            for filename in "${project}"/**/*.c; do
                filenames+=("$(basename -s .c "${filename// /$ }")")
            done

            echo "build out/bin/${project_name}: link $"

            # shellcheck disable=SC2068
            for filename in ${filenames[@]}; do
                echo "    out/build/${project_name}/${filename}.o $"
            done

            echo ""
            echo "build ${project_name}: phony out/bin/${project_name}"
            echo "default out/bin/${project_name}"
            echo ""

            # shellcheck disable=SC2068
            for filename in ${filenames[@]}; do
                echo "build out/build/${project_name}/${filename}.o: cc src/${project_name}/${filename}.c"
            done
            echo ""
        fi
    done

    IFS="${OIFS}"
}

main > targets.ninja
