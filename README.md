# nvfand

nvfand is a daemon that sets the fan speed of your GPU(s) based on a custom curve.

## Config

The config path can be passed as first argument. If no argument is passed the default is `/etc/nvfand.json`.

Schema:

```yaml
{
    "curves": {
        // curve name
        "$key": "string",
        // list of points
        "$value": [
            {
                // temperature associated with the current point
                "temp": "integer",
                // speed associated to the current point
                "speed": "integer"
            }
        ]
    },
    "gpus": {
        // UUID of the gpu
        "$key": "string",
        // name of the curve
        "$value": "string"
    },
    // temperature polling interval in seconds
    "interval": "integer"
}
```

Example:

```yaml
{
    "curves": {
        "name-of-curve": [
            {
                "temp": 0,
                "speed": 25
            },
            {
                "temp": 60,
                "speed": 25
            },
            {
                "temp": 80,
                "speed": 80
            }
        ]
    },
    "gpus": {
        "gpu-UUID": "name-of-curve"
    },
    "interval": 1
}
```

## How does it work
nvfand tries to emulate existing GPU tools for windows, so the config is interpreted as follows:
- imagine a curve as a graph with temps on the x axis and speeds on the y axis
- if the current temp is less than the first point then speed = 0
- if the current temp is higher than the last point then speed = speed of last point.
- else speed = linear interpolation between two points

## Additions
- **nvsetfanspeed**: a CLI tool to set the fan speed manually

## Building
- Install the dependencies
    - nvidia-utils
    - jansson
    - cuda (make)
    - ninja (make)
- Run `./gen_ninja_config.sh`
- Run `ninja`
- Check the output in out/bin

You can customize the build a bit by passing some environment variables to the `gen_ninja_config.sh` script (most useful for development):
- `CC/CFLAGS/LDFLAGS`: the usual
- `NVML_HEADER_PATH`: path to the cuda header
- `SANITIZE`: (requires clang)
    - "memory" to enable memory sanitizer
    - "address" to enable address, undefined, integer and nullability sanitizers
- `DEBUG`: set to 1 to enable debug flags, `SANITIZE` implies `DEBUG`
- `COVERAGE`: (requires clang) set to 1 to enable profiling for coverage

## Development

Some other useful targets:
- `run`: compile and run the daemon (ensure you have a nvfand.json in the current directory)
- `test`: run tests, used only to check that there are no leak (SANITIZE=address) when config parsing fails
- `coverage`: (requires clang) show coverage info, requires a test run when compile with COVERAGE=1
- `format`: self-explanatory

How to...
- generate a compile_commands.json? `ninja -t compdb cc > compile_commands.json`
- clean the project? `ninja -t clean`

Remember to run `./gen_ninja_targets.sh` if files are added/removed/moved.
