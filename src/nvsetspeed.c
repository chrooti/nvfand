#include <getopt.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <nvml.h>

#define PRINT_NVML_ERROR(function) \
    fprintf(stderr, #function " error: %s\n", nvmlErrorString(nvml_ret))

void set_gpu_speed(nvmlDevice_t device, unsigned int speed) {
    nvmlReturn_t nvml_ret;

    unsigned int fan_count;
    nvml_ret = nvmlDeviceGetNumFans(device, &fan_count);
    if (nvml_ret != NVML_SUCCESS) {
        PRINT_NVML_ERROR(nvmlDeviceGetName);
        return;
    }

    unsigned int min_speed;
    unsigned int max_speed;
    nvml_ret = nvmlDeviceGetMinMaxFanSpeed(device, &min_speed, &max_speed);
    if (nvml_ret != NVML_SUCCESS) {
        PRINT_NVML_ERROR(nvmlDeviceGetMinMaxFanSpeed);
        return;
    }

    if (speed < min_speed) {
        speed = min_speed;
    } else if (speed > max_speed) {
        speed = max_speed;
    }

    for (unsigned int i = 0; i < fan_count; ++i) {
        nvml_ret = nvmlDeviceSetFanSpeed_v2(device, i, speed);
        if (nvml_ret != NVML_SUCCESS) {
            PRINT_NVML_ERROR(nvmlDeviceSetFanSpeed_v2);
        }
    }
}

void reset_gpu_fan_policy(nvmlDevice_t device) {
    nvmlReturn_t nvml_ret;

    unsigned int fan_count;
    nvml_ret = nvmlDeviceGetNumFans(device, &fan_count);
    if (nvml_ret != NVML_SUCCESS) {
        PRINT_NVML_ERROR(nvmlDeviceGetName);
        return;
    }

    for (unsigned int i = 0; i < fan_count; ++i) {
        nvml_ret = nvmlDeviceSetDefaultFanSpeed_v2(device, i);
        if (nvml_ret != NVML_SUCCESS) {
            PRINT_NVML_ERROR(nvmlDeviceSetDefaultFanSpeed_v2);
        }
    }
}

#define TAB "\t"

int main(int argc, char** argv) {
    /* CLI option parsing */

    char* uuid = NULL;
    unsigned int speed = 0;

    int opt;
    while ((opt = getopt(argc, argv, "hu:")) != -1) {
        switch (opt) {
        case 'h': {
            printf(
                "nvsetspeed [-u UUID] [SPEED]\n"
                "CLI tool to set the speed of nvidia cards.\n"
                "\n" //
                TAB "SPEED" TAB
                "Selected fan speed (in percentage form). Reset "
                "to default policy if 0 or absent\n" //
                TAB "-u UUID" TAB "Set the speed only if the card matches UUID\n"
            );
            if (uuid != NULL) {
                free(uuid);
            }
            return 0;
        }
        case 'u': {
            if (uuid != NULL) {
                free(uuid);
                return 1;
            }

            size_t uuid_len = strlen(optarg) + 1;
            uuid = malloc(uuid_len);
            memcpy(uuid, optarg, uuid_len);

            break;
        }
        default: {
            goto cleanup_init_error;
        }
        }
    }

    for (int i = optind; i < argc; ++i) {
        speed = (unsigned int) strtoul(argv[i], NULL, 10);
    }

    /* Set the speed */

    nvmlReturn_t nvml_ret;

    nvml_ret = nvmlInit();
    if (nvml_ret != NVML_SUCCESS) {
        PRINT_NVML_ERROR(nvmlInit);

        goto cleanup_init_error;
    }

    if (uuid == NULL) {
        unsigned int gpu_count;
        nvml_ret = nvmlDeviceGetCount_v2(&gpu_count);
        if (nvml_ret != NVML_SUCCESS) {
            PRINT_NVML_ERROR(nvmlDeviceSetFanSpeed_v2);
            return 1;
        }

        if (speed == 0) {
            for (unsigned int i = 0; i < gpu_count; ++i) {
                nvmlDevice_t device;
                nvml_ret = nvmlDeviceGetHandleByIndex(i, &device);
                if (nvml_ret != NVML_SUCCESS) {
                    PRINT_NVML_ERROR(nvmlDeviceSetFanSpeed_v2);
                    continue;
                }

                reset_gpu_fan_policy(device);
            }
        } else {
            for (unsigned int i = 0; i < gpu_count; ++i) {
                nvmlDevice_t device;
                nvml_ret = nvmlDeviceGetHandleByIndex(i, &device);
                if (nvml_ret != NVML_SUCCESS) {
                    PRINT_NVML_ERROR(nvmlDeviceSetFanSpeed_v2);
                    continue;
                }

                set_gpu_speed(device, speed);
            }
        }

    } else {
        nvmlDevice_t device;
        nvml_ret = nvmlDeviceGetHandleByUUID(uuid, &device);
        if (nvml_ret != NVML_SUCCESS) {
            PRINT_NVML_ERROR(nvmlDeviceSetFanSpeed_v2);
            free(uuid);
            return 1;
        }

        if (speed == 0) {
            reset_gpu_fan_policy(device);
        } else {
            set_gpu_speed(device, speed);
        }

        free(uuid);
    }

    return 0;

cleanup_init_error:;
    if (uuid != NULL) {
        free(uuid);
    }

    return 1;
}
