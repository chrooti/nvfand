#ifndef NVFAND_GPU_H
#define NVFAND_GPU_H

#include <nvml.h>

#include "config.h"

struct Gpu {
    // non owned
    struct GpuCurveMapping* curve_mapping;

    nvmlDevice_t device;
    unsigned int fan_count;

    unsigned int min_speed;
    unsigned int max_speed;
};

int gpu_init_library(void);
int gpu_init(struct Gpu* gpu, struct GpuCurveMapping* curve_mapping);
int gpu_adjust_temp(struct Gpu* gpu);
int gpu_set_default_fan_policy(struct Gpu* gpu);

#endif
