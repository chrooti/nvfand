#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "gpu.h"
#include "utils.h"

static int is_terminating = 0;

static void sigterm_handler(int signum) {
    (void) signum;

    is_terminating = 1;
}

int main(int argc, char** argv) {
    /* Find out the config path */

    char* config_path;
    if (argc == 1) {
        // no config_path passed -> default to /etc/nvfand.json

        const char default_config_path[] = "/etc/nvfand.json";
        size_t default_config_path_len = sizeof(default_config_path);

        config_path = malloc(default_config_path_len);
        memcpy(config_path, default_config_path, default_config_path_len);
    } else if (argc == 2) {
        // first (and only) argument is the config path

        size_t config_path_len = strlen(argv[1]) + 1;

        config_path = malloc(config_path_len);
        memcpy(config_path, argv[1], config_path_len);
    } else {
        return 1;
    }

    /* Parse the config */

    struct Config config;
    int ret = config_parse(&config, config_path);
    free(config_path);
    if (!ret) {
        return 1;
    }

    /* Init NVML and get the needed gpu info */

    if (!gpu_init_library()) {
        return 1;
    }

    struct Gpu* gpus = malloc(config.mappings_size * sizeof(*gpus));
    for (size_t i = 0; i < config.mappings_size; ++i) {
        if (!gpu_init(&gpus[i], &config.mappings[i])) {
            return 1;
        }
    }

    /* Setup signal handlers */

    struct sigaction sigterm_action = {
        .sa_handler = sigterm_handler,
        .sa_flags = SA_RESTART,
        .sa_mask = {0}};
    sigemptyset(&sigterm_action.sa_mask);
    if (sigaction(SIGINT, &sigterm_action, NULL) == -1) {
        PRINT_SYSTEM_ERROR(sigaction);
        return 1;
    }

    /* Loop */

    while (!is_terminating) {
        for (size_t i = 0; i < config.mappings_size; ++i) {
            gpu_adjust_temp(&gpus[i]);
        }

        sleep(config.interval);
    }

    /* Cleanup */

    for (size_t i = 0; i < config.mappings_size; ++i) {
        gpu_set_default_fan_policy(&gpus[i]);
    }
    CONFIG_FREE(&config);

    return 0;
}
