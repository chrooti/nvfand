#include "gpu.h"

#include <nvml.h>
#include <stdio.h>

#include "config.h"

#define PRINT_NVML_ERROR(function) \
    fprintf(stderr, #function " error: %s\n", nvmlErrorString(nvml_ret))

int gpu_init_library(void) {
    nvmlReturn_t nvml_ret;

    nvml_ret = nvmlInit();
    if (nvml_ret != NVML_SUCCESS) {
        PRINT_NVML_ERROR(nvmlInit);
        return 0;
    }

    return 1;
}

int gpu_init(struct Gpu* gpu, struct GpuCurveMapping* curve_mapping) {
    nvmlReturn_t nvml_ret;
    gpu->curve_mapping = curve_mapping;

    nvml_ret = nvmlDeviceGetHandleByUUID(curve_mapping->uuid, &gpu->device);
    if (nvml_ret != NVML_SUCCESS) {
        PRINT_NVML_ERROR(nvmlDeviceGetHandleByUUID);
        return 0;
    }

    nvml_ret = nvmlDeviceGetNumFans(gpu->device, &gpu->fan_count);
    if (nvml_ret != NVML_SUCCESS) {
        PRINT_NVML_ERROR(nvmlDeviceGetName);
        return 0;
    }

    nvml_ret = nvmlDeviceGetMinMaxFanSpeed(gpu->device, &gpu->min_speed, &gpu->max_speed);
    if (nvml_ret != NVML_SUCCESS) {
        PRINT_NVML_ERROR(nvmlDeviceGetMinMaxFanSpeed);
        return 0;
    }

    return 1;
}

int gpu_adjust_temp(struct Gpu* gpu) {
    nvmlReturn_t nvml_ret;

    /* Get the current temp */

    unsigned int temp;
    nvml_ret = nvmlDeviceGetTemperature(gpu->device, NVML_TEMPERATURE_GPU, &temp);
    if (nvml_ret != NVML_SUCCESS) {
        PRINT_NVML_ERROR(nvmlDeviceGetName);
        return 0;
    }

    /* Find out the correct speed */

    float speed_f;
    struct FanCurve* curve = gpu->curve_mapping->curve;

    if (temp < curve->points[0].temp) {
        // temp lower than the first point -> speed = 0
        speed_f = 0;
    } else {
        // find the first point (starting from the second one) that has a higher temp
        // than the current one
        struct FanCurvePoint* previous_point = &curve->points[0];
        for (size_t i = 1; i < curve->points_size; ++i) {
            struct FanCurvePoint* current_point = &curve->points[i];
            if (temp < current_point->temp) {
                // apply linear interpolation between the current and the previous point
                // to find out the wanted speed
                speed_f = (float) previous_point->speed
                    + current_point->lerp_factor * (float) (temp - previous_point->temp);

                goto speed_found;
            }
            previous_point = current_point;
        }
        /* else */ {
            // if we reach the end the speed is equal to the last point
            speed_f = (float) previous_point->speed;
        }
    speed_found:;
    }

    /* Convert the speed into unsigned and clip it */

    unsigned int speed_u = (unsigned int) speed_f;
    if (speed_u < gpu->min_speed) {
        speed_u = gpu->min_speed;
    } else if (speed_u > gpu->max_speed) {
        speed_u = gpu->max_speed;
    }

    /* Set the correct speed and log it */

    unsigned int current_speed;
    nvmlDeviceGetFanSpeed_v2(gpu->device, 0, &current_speed);

    // a bit of tolerance
    if (speed_u == current_speed || speed_u == current_speed + 1
        || speed_u == current_speed - 1) {
        return 1;
    }

    fprintf(
        stdout,
        "UUID: %.*s, Temp: %d, Current speed: %u, chosen speed: %u\n",
        (int) gpu->curve_mapping->uuid_len,
        gpu->curve_mapping->uuid,
        temp,
        current_speed,
        speed_u
    );

    for (unsigned int i = 0; i < gpu->fan_count; ++i) {
        nvml_ret = nvmlDeviceSetFanSpeed_v2(gpu->device, i, speed_u);
        if (nvml_ret != NVML_SUCCESS) {
            PRINT_NVML_ERROR(nvmlDeviceSetFanSpeed_v2);
            return 0;
        }
    }

    return 1;
}

int gpu_set_default_fan_policy(struct Gpu* gpu) {
    for (unsigned int i = 0; i < gpu->fan_count; ++i) {
        nvmlReturn_t nvml_ret = nvmlDeviceSetDefaultFanSpeed_v2(gpu->device, i);
        if (nvml_ret != NVML_SUCCESS) {
            PRINT_NVML_ERROR(nvmlDeviceSetDefaultFanSpeed_v2);
            return 0;
        }
    }

    return 1;
}
