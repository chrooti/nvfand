#include "config.h"

#include <stdlib.h>
#include <sys/stat.h>

#include <jansson.h>

#include "utils.h"

// avoids the dependency on nvml.h
#define NVML_DEVICE_UUID_V2_BUFFER_SIZE 96

#define PRINT_CONFIG_ERROR(error, ...) \
    fprintf(stderr, "Config error: " error "\n", ##__VA_ARGS__)

#define JSON_OBJECT_GETN_STR(obj, key) json_object_getn(obj, key, sizeof(key) - 1)

/* Free */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"

void config_free_from_stage(
    struct Config* config,
    enum ConfigErrorStage last_successful_stage
) {
    switch (last_successful_stage) {
    case CONFIG_ERROR_STAGE_NONE:
    case CONFIG_ERROR_STAGE_MAPPINGS_CURVE_NAME:
    case CONFIG_ERROR_STAGE_MAPPINGS_UUID: {
        config->mappings_size -=
            CONFIG_ERROR_STAGE_MAPPINGS_CURVE_NAME > last_successful_stage;

        for (size_t i = 0; i < config->mappings_size; ++i) {
            struct GpuCurveMapping* mapping = &config->mappings[i];
            free(mapping->uuid);
        }

        free(config->mappings);
    }
    case CONFIG_ERROR_STAGE_CURVES_POINTS:
    case CONFIG_ERROR_STAGE_CURVES_NAME: {
        for (size_t i = 0; i < config->curves_size; ++i) {
            struct FanCurve* curve = &config->curves[i];
            free(curve->name);
        }

        // if we're starting after the "name" stage the name for the last iteration
        // has already been allocated, so we have to clear it
        config->curves_size -= CONFIG_ERROR_STAGE_CURVES_POINTS > last_successful_stage;

        for (size_t i = 0; i < config->curves_size; ++i) {
            struct FanCurve* curve = &config->curves[i];
            free(curve->points);
        }

        free(config->curves);
    }
    }
}

#pragma GCC diagnostic pop

/* Parsing */

void config_print_json_parse_error(
    json_error_t* error,
    const char* buffer,
    const char* buffer_end
) {
    // TODO: counts are wrong if there are multibyte characters
    // see: https://jansson.readthedocs.io/en/latest/apiref.html#error-reporting

    const char* error_char = &buffer[error->position];

    const char* error_line_ptr = error_char;
    while (error_line_ptr != buffer && *error_line_ptr != '\n') {
        error_line_ptr--;
    }

    const char* error_line_end_ptr = strchr(error_char, '\n');
    if (error_line_end_ptr == NULL) {
        error_line_end_ptr = buffer_end;
    }
    int error_line_len = (int) (error_line_end_ptr - error_line_ptr);

    char* arrow_buffer = calloc(1, (unsigned long) error->column);
    memset(arrow_buffer, '-', (unsigned long) error->column - 1);
    arrow_buffer[error->column - 1] = '^';

    fprintf(
        stderr,
        "Error while parsing json at line %d column %d:\n"
        "%.*s\n"
        "%.*s\n"
        "%s\n",
        error->line,
        error->column,
        error_line_len,
        error_line_ptr,
        error->column,
        arrow_buffer,
        error->text
    );

    free(arrow_buffer);
}

int config_parse_curve_point(struct FanCurvePoint* point, json_t* point_json) {
    json_t* temp_json = JSON_OBJECT_GETN_STR(point_json, "temp");
    if (!json_is_integer(temp_json)) {
        return 0;
    }
    point->temp = (unsigned int) json_integer_value(temp_json);

    json_t* speed_json = JSON_OBJECT_GETN_STR(point_json, "speed");
    if (!json_is_integer(speed_json)) {
        return 0;
    }
    point->speed = (unsigned int) json_integer_value(speed_json);

    return 1;
}

enum ConfigErrorStage config_parse_curve(
    struct FanCurve* curve,
    const char* name,
    size_t name_len,
    json_t* curve_points_json
) {
    /* Parse the curve name */

    curve->name_len = name_len;
    curve->name = malloc(name_len);
    memcpy(curve->name, name, name_len);

    /* Parse the points */

    size_t curve_points_size = json_array_size(curve_points_json);
    if (curve_points_size == 0) {
        PRINT_CONFIG_ERROR("curve \"%.*s\" has zero points", (int) name_len, name);
        return CONFIG_ERROR_STAGE_CURVES_NAME;
    }
    curve->points_size = curve_points_size;
    curve->points = malloc(curve_points_size * sizeof(*curve->points));

    // first curve point has no previous point, so it gets special treatment
    struct FanCurvePoint* first_curve_point = &curve->points[0];
    json_t* first_curve_point_json = json_array_get(curve_points_json, 0);
    if (!config_parse_curve_point(first_curve_point, first_curve_point_json)) {
        PRINT_CONFIG_ERROR(
            "curve \"%.*s\" point number 0 is invalid",
            (int) name_len,
            name
        );

        return CONFIG_ERROR_STAGE_CURVES_POINTS;
    }
    first_curve_point->lerp_factor =
        (float) first_curve_point->speed / (float) first_curve_point->temp;

    struct FanCurvePoint* previous_curve_point = first_curve_point;
    for (size_t i = 0; i < curve_points_size; ++i) {
        struct FanCurvePoint* current_curve_point = &curve->points[i];
        json_t* current_curve_point_json = json_array_get(curve_points_json, i);
        if (!config_parse_curve_point(current_curve_point, current_curve_point_json)) {
            PRINT_CONFIG_ERROR(
                "curve \"%.*s\" point number %zu is invalid",
                (int) name_len,
                name,
                i
            );

            return CONFIG_ERROR_STAGE_CURVES_POINTS;
        }

        // check that x1 < x2 to be sure that the points are in order
        // this is not strictly necessary but ensures the curve works as intended
        if (current_curve_point->temp < previous_curve_point->temp) {
            PRINT_CONFIG_ERROR(
                "curve \"%.*s\" point number %zu is not ordered",
                (int) name_len,
                name,
                i
            );

            return CONFIG_ERROR_STAGE_CURVES_POINTS;
        }

        // int casting to avoid negative overflow (negative lerp factor is allowed)
        current_curve_point->lerp_factor =
            (float) ((int) current_curve_point->speed - (int) previous_curve_point->speed)
            / (float) (current_curve_point->temp - previous_curve_point->temp);

        previous_curve_point = current_curve_point;
    }

    return CONFIG_ERROR_STAGE_NONE;
}

enum ConfigErrorStage config_parse_mapping(
    struct GpuCurveMapping* mapping,
    struct FanCurve* curves,
    size_t curves_size,
    const char* gpu_uuid,
    size_t gpu_uuid_len,
    json_t* curve_name_json
) {
    /* Parse the gpu name */

    if (gpu_uuid_len > NVML_DEVICE_UUID_V2_BUFFER_SIZE) {
        PRINT_CONFIG_ERROR(
            "gpu uuid \"%.*s\" exceeds maximum length %d",
            (int) gpu_uuid_len,
            gpu_uuid,
            NVML_DEVICE_UUID_V2_BUFFER_SIZE
        );
        return CONFIG_ERROR_STAGE_MAPPINGS_UUID;
    }
    mapping->uuid_len = gpu_uuid_len;
    mapping->uuid = malloc(gpu_uuid_len + 1);
    memcpy(mapping->uuid, gpu_uuid, gpu_uuid_len);
    mapping->uuid[gpu_uuid_len] = 0;

    /* Parse the associated curve name */

    size_t config_name_len = json_string_length(curve_name_json);
    if (config_name_len == 0) {
        PRINT_CONFIG_ERROR(
            "invalid config name for gpu_uuid \"%.*s\"",
            (int) gpu_uuid_len,
            gpu_uuid
        );
        return CONFIG_ERROR_STAGE_MAPPINGS_CURVE_NAME;
    }

    const char* curve_name = json_string_value(curve_name_json);
    for (size_t i = 0; i < curves_size; ++i) {
        struct FanCurve* curve = &curves[i];
        if (strnncmp(curve->name, curve_name, curve->name_len, config_name_len) == 0) {
            mapping->curve = curve;
            return CONFIG_ERROR_STAGE_NONE;
        }
    }
    /* else */ {
        PRINT_CONFIG_ERROR(
            "config name \"%.*s\" for gpu uuid \"%.*s\" does not exist",
            (int) config_name_len,
            curve_name,
            (int) gpu_uuid_len,
            gpu_uuid
        );
        return CONFIG_ERROR_STAGE_MAPPINGS_CURVE_NAME;
    }
}

int config_parse(struct Config* config, const char* path) {
    /* Read the file */

    struct stat stats;
    if (stat(path, &stats) == -1) {
        PRINT_SYSTEM_ERROR(stat);
        return 0;
    }
    if (stats.st_size == 0) {
        // config_print_json_parse_error expects a non-empty buffer
        // so let's just avoid all the alloc/parsing machinery if the file is empty
        PRINT_CONFIG_ERROR("Empty file");
        return 0;
    }
    size_t file_size = (size_t) stats.st_size;

    FILE* file = fopen(path, "r");
    if (file == NULL) {
        PRINT_SYSTEM_ERROR(fopen);
        return 0;
    }

    char* buffer = malloc(file_size + 1);
    const char* buffer_end = buffer + file_size + 1;

    unsigned long ret = fread(buffer, file_size, 1, file);
    if (ret != file_size && ferror(file) != 0) {
        PRINT_SYSTEM_ERROR(fread);
        return 0;
    }

    buffer[stats.st_size] = '\0';
    fclose(file);

    /* Parse the json */

    json_error_t error;
    json_t* root_json = json_loadb(buffer, file_size, JSON_REJECT_DUPLICATES, &error);
    if (root_json == NULL) {
        config_print_json_parse_error(&error, buffer, buffer_end);
        free(buffer);

    cleanup_on_error:;
        json_decref(root_json);
        return 0;
    }
    free(buffer);

    if (!json_is_object(root_json)) {
        PRINT_CONFIG_ERROR("expected object as root element");
        goto cleanup_on_error;
    }

    /* Map the curve definitions to structs */

    json_t* curves_json = JSON_OBJECT_GETN_STR(root_json, "curves");
    size_t curves_size = json_object_size(curves_json);
    if (curves_size == 0) {
        PRINT_CONFIG_ERROR("missing \"curves\" key");
        goto cleanup_on_error;
    }
    config->curves = malloc(curves_size * sizeof(*config->curves));
    config->curves_size = 0;

    {
        const char* curve_name;
        size_t curve_name_len;
        json_t* curve_points_json;
        json_object_keylen_foreach(
            curves_json,
            curve_name,
            curve_name_len,
            curve_points_json
        ) {
            struct FanCurve* curve = &config->curves[config->curves_size];
            config->curves_size++;

            enum ConfigErrorStage error_stage =
                config_parse_curve(curve, curve_name, curve_name_len, curve_points_json);
            if (error_stage != CONFIG_ERROR_STAGE_NONE) {
                config_free_from_stage(config, error_stage);
                goto cleanup_on_error;
            }
        }
    }

    /* Map the gpu to curve mappings to structs */

    json_t* mappings_json = JSON_OBJECT_GETN_STR(root_json, "gpus");
    size_t mappings_size = json_object_size(mappings_json);
    if (mappings_size == 0) {
        PRINT_CONFIG_ERROR("missing \"gpus\" key");
        config_free_from_stage(config, CONFIG_ERROR_STAGE_CURVES_POINTS);
        goto cleanup_on_error;
    }
    config->mappings = malloc(mappings_size * sizeof(*config->mappings));
    config->mappings_size = 0;

    {
        const char* gpu_uuid;
        size_t gpu_uuid_len;
        json_t* curve_name_json;
        json_object_keylen_foreach(
            mappings_json,
            gpu_uuid,
            gpu_uuid_len,
            curve_name_json
        ) {
            struct GpuCurveMapping* mapping = &config->mappings[config->mappings_size];
            config->mappings_size++;

            enum ConfigErrorStage error_stage = config_parse_mapping(
                mapping,
                config->curves,
                config->curves_size,
                gpu_uuid,
                gpu_uuid_len,
                curve_name_json
            );

            if (error_stage != CONFIG_ERROR_STAGE_NONE) {
                config_free_from_stage(config, error_stage);
                goto cleanup_on_error;
            }
        }
    }

    /* Handle other options */

    json_t* interval_json = JSON_OBJECT_GETN_STR(root_json, "interval");
    if (interval_json != NULL) {
        unsigned int interval = (unsigned int) json_integer_value(interval_json);
        if (interval == 0) {
            fprintf(stderr, "\"interval\" value must be an integer and != 0\n");
            CONFIG_FREE(config);
            goto cleanup_on_error;
        }
        config->interval = interval;
    } else {
        config->interval = 1;
    }

    return 1;
}
